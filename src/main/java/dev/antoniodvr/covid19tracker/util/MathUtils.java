package dev.antoniodvr.covid19tracker.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class MathUtils {

    private MathUtils() {

    }

    public static <T> Double calculatePercentageChange(List<T> list, Function<T, Integer> function) {
        if(list.size() <= 1) {
            return 0D;
        }

        Integer currentValue = function.apply(list.get(0));
        Integer previousValue = function.apply(list.get(1));
        return MathUtils.calculatePercentageChange(currentValue, previousValue);
    }

    public static <T> Integer diff(List<T> list, Function<T, Integer> function) {
        if(list.isEmpty()) {
            return 0;
        } else if(list.size() == 1) {
            return function.apply(list.get(0));
        }

        Integer currentValue = function.apply(list.get(0));
        Integer previousValue = function.apply(list.get(1));
        return currentValue-previousValue;
    }

    public static List<Double> calculateValuesPercentage(Integer ... values) {
        BigDecimal sum = BigDecimal.valueOf(Stream.of(values).mapToDouble(Double::valueOf).sum());
        List<Double> valuesPercentage = new ArrayList<>();
        for(Integer value : values) {
            double valuePercentage = 0D;

            if(value != 0) {
                BigDecimal divisor = sum.divide(BigDecimal.valueOf((double) value), 2, RoundingMode.HALF_UP);
                valuePercentage = BigDecimal.valueOf(100)
                        .divide(divisor, 2, RoundingMode.HALF_UP)
                        .setScale(2, RoundingMode.HALF_UP)
                        .doubleValue();
            }

            valuesPercentage.add(valuePercentage);
        }
        return valuesPercentage;
    }

    public static Double calculatePercentageChange(Integer currentValue, Integer previousValue) {
        if(previousValue == 0) {
            return Double.NaN;
        }

        BigDecimal previousValueDecimal = BigDecimal.valueOf(previousValue);
        return (BigDecimal.valueOf(currentValue).subtract(previousValueDecimal))
                .multiply(BigDecimal.valueOf(100))
                .divide(previousValueDecimal, 2, RoundingMode.HALF_UP)
                .setScale(2, RoundingMode.HALF_UP)
                .doubleValue();
    }
}
